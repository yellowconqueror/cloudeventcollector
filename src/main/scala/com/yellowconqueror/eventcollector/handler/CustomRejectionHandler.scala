package com.yellowconqueror.eventcollector.handler

import spray.routing.{RejectionHandler, MissingHeaderRejection, HttpService}

/**
 * Created by shuang on 6/19/14.
 */
trait CustomRejectionHandler extends HttpService{
  implicit val myRejectionHandler = RejectionHandler {
    case MissingHeaderRejection(_) :: _ =>
      complete(400,"Missing Headers")
  }


}

