package com.yellowconqueror.eventcollector.handler

import java.io.IOException

import akka.actor.ActorLogging
import spray.routing.{HttpService, ExceptionHandler}
import spray.util.LoggingContext

/**
 * Created by shuang on 6/19/14.
 */
trait CustomExceptionHandler extends HttpService {
  implicit val myExceptionHandler = ExceptionHandler {
    case e:IOException =>
      complete(500,e)
    case e:Exception =>
      complete(500,e)
  }

}

