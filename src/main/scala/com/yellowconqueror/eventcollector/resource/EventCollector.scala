package com.yellowconqueror.eventcollector.resource

import java.util.UUID
import java.util.concurrent.TimeUnit

import akka.actor.{ActorRef, ActorLogging, Props, Actor}
import akka.routing.{FromConfig, SmallestMailboxPool, SmallestMailboxRoutingLogic}
import akka.util.Timeout

import com.yellowconqueror.eventcollector.MessageProtocol.{EventWritten, ResponderArg, WriteEventRequest, Event}
import com.yellowconqueror.eventcollector.handler.{CustomExceptionHandler, CustomRejectionHandler}
import com.yellowconqueror.eventcollector.worker.EventFileWriterActor
import com.typesafe.config.ConfigFactory
import spray.httpx.SprayJsonSupport
import spray.routing._
import spray.http._
import MediaTypes._
import akka.pattern.{ask,pipe}
import SprayJsonSupport._

import scala.concurrent.ExecutionContext
import ExecutionContext.Implicits.global

import scala.concurrent.duration.Duration

// we don't implement our route structure directly in the service actor because
// we want to be able to test it independently, without having to spin up an actor
class EventCollectorActor extends HttpServiceActor with EventCollector with CustomRejectionHandler with CustomExceptionHandler with ActorLogging{

  def receive = runRoute(handleRejections(myRejectionHandler)(handleExceptions(myExceptionHandler)(myRoute)))

}


// this trait defines our service behavior independently from the service actor
trait EventCollector extends HttpService with ActorLogging { actor: Actor =>
  import com.yellowconqueror.eventcollector.MessageProtocol.EventJsonSupport._
  implicit val timeout = Timeout(Duration(10L, TimeUnit.SECONDS))
  val config = ConfigFactory.load()
  val fileWriterRouter:ActorRef = context.actorOf(FromConfig.props(Props[EventFileWriterActor]), "fileWriterRouter")
  val responderRouter:ActorRef = context.actorOf(FromConfig.props(Props[Responder]), "responderRouter")
  val myRoute =
    path("") {
      get {
        respondWithMediaType(`text/html`) { // XML is marshalled to `text/xml` by default, so we simply override here
          complete {
            <html>
              <body>
                <h1>Say hello to <i>spray-routing</i> on <i>spray-can</i>!</h1>
              </body>
            </html>
          }
        }
      }
    }~
    path("putevent" / PathMatchers.Segment) { tenantId =>
      put {
        headerValueByName("regKey") { regKey =>
          entity(as[Event]) { event => requestContext =>
            implicit val timeout = Timeout(Duration(10L, TimeUnit.SECONDS))
            fileWriterRouter.ask(WriteEventRequest(UUID.randomUUID(),event, tenantId,requestContext)).pipeTo(responderRouter)
          }
        }
      }
    }
}

