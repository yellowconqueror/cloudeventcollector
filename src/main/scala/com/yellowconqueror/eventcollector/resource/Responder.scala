package com.yellowconqueror.eventcollector.resource

import akka.actor.Actor
import com.yellowconqueror.eventcollector.MessageProtocol.{ResponderArg, EventWritten}
import com.yellowconqueror.eventcollector.MessageProtocol.EventWrittenJsonSupport._
import spray.http.StatusCodes
import spray.routing.RequestContext

/**
 * Created by shuang on 6/16/14.
 */
class Responder extends Actor{
  def receive = {
    case response:ResponderArg =>
      response.requestContext.complete(StatusCodes.Accepted,response.eventWritten)
  }

}

