package com.yellowconqueror.eventcollector

import java.util.UUID

import spray.httpx.SprayJsonSupport
import spray.routing.RequestContext


/**
 * Created with IntelliJ IDEA.
 * User: shuang
 * Date: 6/13/14
 * Time: 11:09 PM
 * To change this template use File | Settings | File Templates.
 */
object MessageProtocol {
  import spray.json._

  case class Event(
                    eventType:String,
                    eventTime:Long,
                    personId:String,
                    sourceAddressId:String,
                    channelId:String,
                    agentUser:String,
                    agentChannelId:String,
                    classifier:String,
                    productId:String,
                    deviceManufacturer:String,
                    deviceModel:String,
                    deviceOs:String,
                    deviceOsVersion:String,
                    correlationId:String,
                    nodeId:String,
                    userAgentString:String,
                    eventVersion:String,
                    eventSource:String);

  case class EventWritten(eventId:String)
  case class ResponderArg(eventWritten:EventWritten,requestContext:RequestContext)
  case class WriteEventRequest(uuid:UUID,event:Event,tenant:String,requestContext:RequestContext)

  object EventJsonSupport extends DefaultJsonProtocol with SprayJsonSupport {
    implicit val PortofolioFormats = jsonFormat18(Event)
  }

  object EventWrittenJsonSupport extends DefaultJsonProtocol with SprayJsonSupport {
    implicit val PortofolioFormats = jsonFormat1(EventWritten)
  }


}

