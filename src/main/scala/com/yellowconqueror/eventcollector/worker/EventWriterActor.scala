package com.yellowconqueror.eventcollector.worker

import akka.actor.{ActorLogging, Actor}
import com.yellowconqueror.eventcollector.MessageProtocol.{ResponderArg, WriteEventRequest, EventWritten, Event}
import java.util.{UUID, Date}
import java.io.{IOException, FileWriter, File}
import java.text.SimpleDateFormat
import com.typesafe.config.ConfigFactory
import spray.json._
import DefaultJsonProtocol._
import com.yellowconqueror.eventcollector.MessageProtocol.EventJsonSupport._
import spray.routing.RequestContext


/**
 * Created with IntelliJ IDEA.
 * User: shuang
 * Date: 6/13/14
 * Time: 10:42 PM
 * To change this template use File | Settings | File Templates.
 */
class EventFileWriterActor extends Actor with ActorLogging{
  val config = ConfigFactory.load()
  val datepath = new SimpleDateFormat("yyyy/MM/dd/HH/mm/ss")
  val baseDir = config.getString("filesystem.datadir")

  def receive: Actor.Receive = {
    case WriteEventRequest(uuid: UUID, event: Event, tenant: String, requestContext: RequestContext) =>
      val now = new Date
      //val uuid = UUID.randomUUID()
      val path = new StringBuilder(this.baseDir)
      path.append(File.separator)
      path.append(tenant)
      path.append(File.separator)
      path.append(datepath.format(now))
      path.append(File.separator)
      log.debug("here is the path: " + path.toString())
      val parent = new File(path.toString());
      log.debug("event type: " + event.eventType)
      if (!parent.exists())
        if (!parent.mkdirs())
          log.debug("failure to create parent directories")
      path.append(uuid);
      path.append(".json");
      val fileWriter = new FileWriter(new File(path.toString))
      fileWriter.write(event.toJson.prettyPrint)
      fileWriter.flush()
      fileWriter.flush
      fileWriter.close


      sender ! ResponderArg(EventWritten(uuid.toString), requestContext)
  }
}

