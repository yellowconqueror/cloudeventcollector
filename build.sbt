organization  := "com.yellowconqueror"

version       := "0.1"

scalaVersion  := "2.11.4"

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")

//resolvers += "spray" at "http://repo.spray.io/"

libraryDependencies ++= {
  val akkaV = "2.3.6"
  val sprayV = "1.3.2"
  Seq(
    "io.spray"            %   "spray-can_2.11"     % sprayV,
    "io.spray"            %   "spray-routing_2.11" % sprayV,
    "io.spray"            %   "spray-testkit_2.11" % sprayV  % "test",
    "io.spray"          % "spray-json_2.11"      % "1.3.1",
    "com.typesafe.akka"   %%  "akka-actor"    % akkaV,
    "com.typesafe.akka"   %%  "akka-testkit"  % akkaV   % "test",
    "org.specs2"          %%  "specs2-core"   % "2.4.9" % "test"
  )
}

Revolver.settings

