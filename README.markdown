## CloudEventCollector Application

This application is built using spray.io and akka actors.  
Spray functionality highlights include routes, json serialization and de-serialization, pulling header values, url values, custom rejection and exception handlers.
Akka functionality highlights include using routers, property file configuration of routers instead of in-code, the ask and pipeto pattern
  

Follow these steps to get started:

1. Git-clone this repository.

2. Change directory into your clone.

3. Open src/main/resources/application.conf and edit the filesystem.datadir property to a valid path on your system

4. Launch SBT:

        $ sbt

5. Compile everything:

        > compile

6. Start the application:

        > re-start

7. Open soapui project located in soapui directory and execute test located under REST Project 1 -> http://localhost:8080 -> sprayresource put event asb -> ASB -> Request 1

8. Stop the application:

        > re-stop

9. Learn more at http://www.spray.io/

10. Start hacking on `src/main/scala/com/example/MyService.scala`


## Additional resources
+ [spray.io](http://spray.io/)
+ [akka.io](http://akka.io/)

## Intellij Integration
Simply run sbt gen-idea to generate the intellij files and then select the directory to open an existing project within intellij. 