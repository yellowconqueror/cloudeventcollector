import AssemblyKeys._ // put this at the top of the file

assemblySettings

jarName in assembly := "eventCollector.jar"

test in assembly := {}

mainClass in assembly := Some("com.yellowconqueror.eventcollector.Boot")

